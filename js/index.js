import {ModalLogin} from './components/modal/modalLogin.js';
import {ModalAddVisit} from './components/modal/modalVisit.js';
import {Visit} from "./components/visit/visit.js";

export class Form {
    constructor(props) {
        this.props = {...props}
    }
    static serializeJSON () {
        const selectDoctor = document.querySelector('.selectDoctor');
        const newObject = {content:{doctor:selectDoctor.value}};
        const inputForm = document.querySelectorAll('.appointment__select');
        inputForm.forEach(item =>  {
            newObject.content[item.name] = item.value
        });
        return JSON.stringify(newObject);
    }

    static chooseDoctor () {

        const selectDoctor = document.getElementById('getDoctor');
        const placeToRender = document.querySelector('.selectDoctor');
        selectDoctor.addEventListener('change', function (event) {
            const inputFormWrapper = document.getElementsByClassName('form__body');
            if (inputFormWrapper.length === 1) {
                inputFormWrapper[0].remove();
            }
            const visitInfo = {};
            if (event.target.value === 'therapist') {
                const therapistForm = new FormTherapist(visitInfo);
                therapistForm.render(placeToRender);
            }
            else if (event.target.value === 'dentist') {
                const dentistForm = new FormDentist(visitInfo);
                dentistForm.render(placeToRender);
            }
            else if (event.target.value === 'cardiologist') {
                const cardiologistForm = new FormCardiologist(visitInfo);
                cardiologistForm.render(placeToRender)
            }
        })
    }

    render (container) {
        const {className, id,} = this.props;
        const newForm = document.createElement('div');
        newForm.className = className;
        newForm.id = 'form-submit';
        newForm.innerHTML = `      
        <div class="form__body">
            <form id="" name="appointment__form" class="appointment">
                <label class="form-label">Full name:</label>
                    <input type="text" required class="appointment__select" name="fullname" placeholder="Name" value="Bobrov Ivan">
                <label class="form-label">Purpose:</label>
                    <input type="text" required class="appointment__select" name="title" placeholder="Purpose" value="Planning visit">
                <label class="form-label">Description:</label>
                    <textarea  class="appointment__select description" name="description" placeholder="Description"
                    id="description" rows="3" cols="40">Feeling not good, have high temperature</textarea>
                <label class="form-label">Urgency:
                    <select id="choose-priority" class="appointment__select priority" name="urgency" required>
                        <option disabled>Select</option>
                        <option>Low</option>
                        <option selected>Middle</option>
                        <option>High</option>
                    </select>
                    </label>
                    <input class="appointment__button btn" id="submit-visit" value="Create visit" type="submit">
            </form>
        </div>`;
        container.after(newForm);

        const submitForm = document.getElementById('form-submit');
        submitForm.addEventListener('submit', function (e) {
            e.preventDefault();
            const token = localStorage.getItem('token');
            const modalWindow = document.querySelector('.modal');
            if (modalWindow.id === 'modal-visit') {
                const visitFullInfo = Form.serializeJSON();

                fetch('http://cards.danit.com.ua/cards', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: visitFullInfo
                })
                    .then(response => {
                        if(!response.ok) {
                            throw new Error('что-то поломалоcь')
                        }
                        return response.json()
                    })
                    .then(data => {
                        const visit = new Visit(data);
                        visit.render();

                    })
                    .catch(error => {
                        console.log(error);
                    });

            }
            else if (modalWindow.id === 'modal-edit') {
                // необходимо сделать   put запрос
            }
            modalWindow.classList.remove('active');
        })
    }
}

export class FormCardiologist extends Form{
    constructor(visit) {
        super(visit);
    }
    render(container) {
        super.render(container);
        const visitInfo = document.querySelector('#choose-priority');
        visitInfo.insertAdjacentHTML('afterend', `
        <label class="form-label">BMI:
             <input type="number"  required class="appointment__select" name="bmi" value="20" min="0" max="500" >
             </label>
        <label class="form-label">BP:
             <input type="number"  required class="appointment__select" name="bp" value="130" min="0" max="200" >
             </label>
        <label class="form-label">AGE:
            <input type="number"  required class="appointment__select" name="age" value="25" min="0" max="100" >
            </label>
        <label class="form-label">History:  
            <textarea  class="appointment__select" name="history" placeholder="Description" id="description"
            rows="3" cols="40" >Have some problem with heart arythmy</textarea>
            </label>`)
    }
}

export class FormDentist extends Form {
    constructor(visit) {
        super(visit);
    }
    render(container) {
        super.render(container);
        const visitInfo = document.querySelector('#choose-priority');
        visitInfo.insertAdjacentHTML('afterend', `
            <label class="form-label">Last date of visit:</label>
                <input type="date" required class="appointment__select" id="visit-date" name="visited" value="2020-06-20">
       `);
    }
}
export class FormTherapist extends Form {
    constructor(visit) {
        super(visit);
    }
    render(container) {
        super.render(container);
        const visitInfo = document.querySelector('#choose-priority');
        visitInfo.insertAdjacentHTML('afterend', `
          <label class="form-label">AGE:
            <input type="number"  required class="appointment__select" name="age" value="25" min="0" max="100" >
          </label>
       `)
    }
}

function logIn() {
    const LoginModalProps = {
        className: "modal",
        id: "my-modal",
        text: "Please login",
    };

    const modal = new ModalLogin(LoginModalProps);
    document.body.prepend(modal.render());

    const buttonOpenModal = document.getElementById("headerBtn");
    buttonOpenModal.addEventListener("click", modal.open);

    const registerForm = document.getElementById("formId");
    registerForm.addEventListener('submit', function (e) {
        e.preventDefault();
        const email = document.getElementById('email').value;
        const password = document.getElementById('password').value;
        const body = JSON.stringify({email, password});
        const loginRequest = fetch('http://cards.danit.com.ua/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body
        });
        loginRequest.then(response => {
            if (!response.ok) {
                throw new Error('Page not found');
            }
            return response.json();

        }).then(result => {
            if (result.status === 'Success') {
                this.insertAdjacentHTML('afterend', `<span>Success registration!</span>`);
                localStorage.setItem("token", result.token);
                buttonOpenModal.classList.add('hidden');
                const selectDoctor = document.getElementById('selectDoctor');
                selectDoctor.style.display = "inline-block";
                const modalContent = document.getElementById('my-modal');
                modalContent.remove()

                const token = localStorage.getItem('token');
                fetch('http://cards.danit.com.ua/cards', {
                        method: 'GET',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`
                        }
                    }
                ).then(response => {
                    return  response.json()
                }).then(result => {
                        const allCards = [...result]
                        allCards.map(item=> {
                            const visitCard = new Visit(item);
                            visitCard.render();
                        })
                    }
                ).catch(error => {
                    console.log(error);
                })



            }

            else {
                this.insertAdjacentHTML('afterend', '<span>Incorrect login</span>')
            }



        }).catch(error => console.log(error))
    });
}
logIn();



function getDoctor() {
    const selectDoctorProps = {
        className: "modal",
        id: "modal-visit",
        text: "Создайте визит врача",
    };

    const  modalSelectDoctor = new ModalAddVisit(selectDoctorProps);
    document.body.prepend( modalSelectDoctor.render());
    const selectDoctor = document.getElementById('selectDoctor');
    selectDoctor.addEventListener('click', modalSelectDoctor.open);
    Form.chooseDoctor()
}
getDoctor();

