class Modal {
    constructor(props) {
        this.props = {...props};
    }

    close = () => {
        this.elem.classList.remove("active");
    };

    open = () => {
        this.elem.classList.add("active");
    };

    getContent() {
        const content = `<div class="modal-content">
                                    <span class="close">&times;</span>
                                    ${this.content}                        
                          </div>`;
        return content;
    }

    render() {
        const {className, id} = this.props;
        const modal = document.createElement("div");
        modal.className = className;
        modal.id = id;
        modal.innerHTML = this.getContent();
        const close = modal.querySelector(".close");
        close.addEventListener("click", this.close);
        this.elem = modal;
        return modal;
    }

}
export default Modal;