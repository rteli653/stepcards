import Modal from "./modal.js";

class ModalEditVisit extends Modal{
    content = ` <h2 class="form-name">Edit visit</h2>
                <select class="selectDoctor" id="getDoctor">
                    <option value="therapist">Therapist</option>
                    <option value="cardiologist">Cardiologist</option>
                    <option value="dentist">Dentist</option>
                </select>`
}
export {ModalEditVisit}