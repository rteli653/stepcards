import Modal from "./modal.js";

class ModalAddVisit extends Modal{
    content = ` <h2 class="form-name">Create visit</h2>
                <select  class="selectDoctor" id="getDoctor">
                    <option>Select a doctor</option>
                    <option value="therapist">Therapist</option>
                    <option value="cardiologist">Cardiologist</option>
                    <option value="dentist">Dentist</option>
                </select>`
}
export {ModalAddVisit}