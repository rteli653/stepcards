import Component from "../component.js";
import Modal from "../modal/modal.js";
import {Form} from "../../index.js"
import {ModalDelete} from "../modal/modalDelete.js";
import {ModalEditVisit} from "../modal/modalEditVisit.js";


export class Visit extends Component{
    render () {
        const {doctor, fullname, urgency, ...attr} = this.props.content
        const noVisit = document.querySelector('.no__visits');
        noVisit ? noVisit.classList.add('hidden') : "";
        const visitCard = document.createElement('div');
        visitCard.classList.add('visit__card');
        visitCard.setAttribute('draggable', 'true');
        visitCard.id = this.props.id;
        visitCard.innerHTML = `<hr class="urgency--line">
                                <h3 class="visit__doctor">${doctor}</h3>
                                <p class="visit__name">${fullname}</p>
                                <a class="btn visit__info" href="#">More Info</a>
                                <div class="moreinfo--wrap"></div>
                                <div class="visit__buttons">
                                <select class="visit__options">
                                <option selected disabled>Options</option>
                                <option value="visit__edit">Edit</option>
                                <option value="visit__delete">Delete</option>
                                </select>
                                </div>`;
        const visitButtons = visitCard.querySelector('.visit__info')
        const infoWrap = visitCard.querySelector('.moreinfo--wrap')
        const urgencyLine = visitCard.querySelector('.urgency--line')
        switch (urgency) {
            case 'High':
                urgencyLine.classList.add('bg--red');
                break;
            case 'Middle':
                urgencyLine.classList.add('bg--yellow');
                break;
        }
        const container = document.querySelector('.visit__doctors');
        container.append(visitCard);
        this.showattr(attr, visitButtons, infoWrap);
        this.editVisit(visitCard.id)
       //return visit;
    }
    showattr (attr, visitButtons, infoWrap) {
        const element = document.createElement('div');
        element.classList.add('visit__moreinfo');
        element.classList.add('info-hidden');
            for(const [key, value] of Object.entries(attr)){
                if(value) {
                    let upKey = key[0].toUpperCase() + key.slice(1);
                    element.insertAdjacentHTML('beforeend',
                        `<span class="moreinfo__title ${key}">${upKey}</span>
                            <p class="moreinfo__desc">${value}</p>`)
                }
            }
        infoWrap.append(element);
        visitButtons.addEventListener('click', (e) =>
            element.classList.toggle('info-hidden')
        )
    }
    editVisit (id) {

        const currentCard = document.getElementById(id);
        currentCard.addEventListener('click', function (e) {
            e.target.addEventListener('change', function (e) {
            const currentDoctor = currentCard.querySelector('.visit__doctor').innerHTML
            if (e.target.value === 'visit__edit') {
                const modalEditProps = {
                    className: "modal",
                    id: "modalEdit",
                    text: "Edit visit",
                }
                const editModal = new ModalEditVisit(modalEditProps);
                document.body.prepend(editModal.render())
                const editModalPlace = document.getElementById('modalEdit');
                editModalPlace.classList.add('active');
                switch (currentDoctor) {
                    case 'therapist' :
                        break;
                    case 'dentist' :
                        break;
                    case 'cardiologist' :
                        break;
                }
            }
            else if (e.target.value === 'visit__delete') {
                Visit.deleteVisit(id, currentCard)
            }
        })
    })}
    static deleteVisit (id, currentCard) {
        const modalDeleteProps = {
            className: "modal",
            id: "modalDelete",
            text: "Delete visit",
        }
        const modalDel = new ModalDelete(modalDeleteProps);
        document.body.prepend(modalDel.render());
        const delModal = document.getElementById('modalDelete');
        delModal.classList.add('active');
        const delBtn = document.getElementById('btnDelete');
        const password = document.getElementById('password').value;
        delBtn.addEventListener('click', function () {
            fetch(`http://cards.danit.com.ua/login/`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({password})
            })
                .then(response => {
                    if (response.ok) {
                        const token = localStorage.getItem('token');
                        fetch(`http://cards.danit.com.ua/cards/${id}`, {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`
                        }
                        })
                        .then(response => {
                            if (response.ok) {
                                currentCard.remove();
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        }
                        )
                    }
                })
                .catch(error => {
                    console.log(error);
                    }
                )
            delModal.classList.remove('active')

        })
    }

    // urgency (urgency) {
    //     const urgencyLine = document.querySelectorAll('.urgency--line')
    //     urgencyLine.forEach(item => {
    //     switch (urgency) {
    //         case 'Low':
    //             item.classList.remove('bg--red');
    //             item.classList.remove('bg--yellow');
    //             item.classList.add('bg--green');
    //             break;
    //         case 'High':
    //             item.classList.remove('bg--green');
    //             item.classList.remove('bg--yellow');
    //             item.classList.add('bg--red');
    //             break;
    //         case 'Middle':
    //             item.classList.remove('bg--green');
    //             item.classList.remove('bg--red');
    //             item.classList.add('bg--yellow');
    //             break;
    //     }
    // })
    // }

}



